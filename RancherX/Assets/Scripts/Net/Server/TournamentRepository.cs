using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RancherShared;
using UnityEngine;

namespace RancherServer
{


    public class TournamentRepository 
    {
        public static TournamentRepository instance;
        public TournamentRepository(){
            if (instance == null) {
                    instance = this ;
            } else if (instance != this) {
                Debug.LogError ("Another instance of " + GetType () + " is already exist! Destroying self...");
                return;
            }
        }

        private TournamentCollection playerTournamentCollection;

        public TournamentCollection GetTournaments (){
            if(playerTournamentCollection == null){
                playerTournamentCollection = new TournamentCollection();
            }
            return playerTournamentCollection;
        }


    }
}

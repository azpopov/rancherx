using UnityEngine;
using Unity.Networking.Transport;
using Unity.Collections;
using RancherShared;
using System.Data;
using System;

namespace RancherServer
{

    public class BaseServer : MonoSingleton<BaseServer>
    {



        public NetworkDriver driver;
        protected NativeList<NetworkConnection> connections;
        private bool isAlive = true;
#if UNITY_EDITOR
        private void Start()
        {
            MonsterRepository.instance = new MonsterRepository();
            Init(5522);
        }
        private void Update()
        {
            UpdateServer();
        }
        private void OnDestroy()
        {
            ShutDown();
        }
#endif

        public virtual void Init(ushort port)
        {
            driver = NetworkDriver.Create();
            var endpoint = NetworkEndPoint.AnyIpv4;
            endpoint.Port = port;
            if (driver.Bind(endpoint) != 0)
            {
                Debug.Log("There was error binding to port " + endpoint.Port);

            }
            else
            {
                driver.Listen();
            }

            connections = new NativeList<NetworkConnection>(2, Allocator.Persistent);
        }

        public virtual void UpdateServer()
        {
            if (!isAlive)
                return;
            driver.ScheduleUpdate().Complete();
            CleanupConnections();
            AcceptNewConnections();
            UpdateMessagePump();
        }

        public virtual void ShutDown()
        {
            if (connections.IsCreated)
            {
                driver.Dispose();
                connections.Dispose();
            }
        }

        private void AcceptNewConnections()
        {
            NetworkConnection c;
            while ((c = driver.Accept()) != default)
            {
                connections.Add(c);
                Debug.Log($"Accepted a Connection");
            }
        }

        private void CleanupConnections()
        {
            for (var i = 0; i < connections.Length; i++)
            {
                if (!connections[i].IsCreated)
                {
                    connections.RemoveAtSwapBack(i);
                    --i;
                }
            }
        }

        protected virtual void UpdateMessagePump()
        {
            DataStreamReader streamReader;

            for (var i = 0; i < connections.Length; i++)
            {
                NetworkEvent.Type cmd;
                while ((cmd = driver.PopEventForConnection(connections[i], out streamReader)) != NetworkEvent.Type.Empty)
                {
                    if (cmd == NetworkEvent.Type.Data)
                    {
                        OnData(ref streamReader, connections[i]);

                    }
                    else if (cmd == NetworkEvent.Type.Disconnect)
                    {
                        Debug.Log("Client Disconnected from server");
                        connections[i] = default;
                    }
                }
                if (streamReader.HasFailedReads)
                {
                    streamReader = default;
                }
            }
        }

        public virtual void OnData(ref DataStreamReader streamReader, NetworkConnection connection)
        {
            NetMessage netMessage = null;
            var opCode = (OpCode)streamReader.ReadByte();
            short guidLength = streamReader.ReadShort();
            using (NativeArray<byte> monsterGuid = new NativeArray<byte>(guidLength, Allocator.Persistent, NativeArrayOptions.ClearMemory))
            {
                streamReader.ReadBytes(monsterGuid);
                var byteGuidArr = NativeArrayExtension.ToRawBytes(monsterGuid);
                var guid = System.Text.Encoding.ASCII.GetString(byteGuidArr, 0, guidLength);
                switch (opCode)
                {
                    case OpCode.GenerateMonsterFromSeed:
                        netMessage = new Net_GenerateMonsterFromSeed(ref streamReader, guid);
                        netMessage.DeSerializeOnServer(ref streamReader);
                        var monsterUpdateGenerate = MonsterRepository.instance.GenerateMonster((netMessage as Net_GenerateMonsterFromSeed)._monsterSeed);
                        SendToClient(connection, new Net_GenerateMonsterFromSeed(monsterUpdateGenerate, guid));
                        break;
                    case OpCode.GetTournaments:
                        netMessage = new Net_GetTournaments(ref streamReader, guid);
                        netMessage.DeSerializeOnServer(ref streamReader);
                        var tournamentUpdate = new TournamentCollection();
                        SendToClient(connection, new Net_GetTournaments(tournamentUpdate));

                        break;

                    case OpCode.GetMonsterCollection:
                        netMessage = new Net_GetMonsters(ref streamReader, guid);
                        netMessage.DeSerializeOnServer(ref streamReader);
                        var monsterUpdateList = MonsterRepository.instance.ranchMonsters;
                        SendToClient(connection, new Net_GetMonsters(monsterUpdateList, guid));
                        break;
                    case OpCode.DeleteMonster:
                        netMessage = new Net_DeleteMonster(ref streamReader, guid);
                        netMessage.DeSerializeOnServer(ref streamReader);
                        var monsterToDelete = (netMessage as Net_DeleteMonster).monsterGuid;
                        System.Collections.Generic.List<Monster> list = MonsterRepository.instance.ranchMonsters.list;
                        list.RemoveAt(list.IndexOf(list.Find(item => item.guid == monsterToDelete)));
                        SendToClient(connection, new Net_DeleteMonster(MonsterRepository.instance.ranchMonsters, guid));
                        break;
                    default:
                        break;
                }
                netMessage.ReceivedOnServer();
            }
        }


        public virtual void Broadcast(NetMessage message)
        {
            foreach (var connection in connections)
            {
                SendToClient(connection, message);
            }
        }

        public virtual void SendToClient(NetworkConnection connection, NetMessage message)
        {
            driver.BeginSend(connection, out DataStreamWriter writer);
            message.SerializeToClient(ref writer);
            driver.EndSend(writer);
        }
    }
}
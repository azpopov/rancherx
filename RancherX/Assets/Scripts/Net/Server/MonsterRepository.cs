using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RancherShared;
using UnityEngine;

namespace RancherServer
{


    public class MonsterRepository 
    {
        public MonsterCollection ranchMonsters;
        public static MonsterRepository instance;

        public MonsterRepository(){
            if (instance == null) {
                instance = this ;
                ranchMonsters = new MonsterCollection();
                ranchMonsters = GenerateMonster(123);
            } else if (instance != this) {
                Debug.LogError ("Another instance of " + GetType () + " is already exist! Destroying self...");
                return;
            }
        }

        IEnumerable<Monster> List (){
            return MonsterRepository.instance.ranchMonsters.list;
        }

        public void OnGenerateRandomMonster()
        {
            var randomSeed = ((int)((new System.Random()).NextDouble() * Int32.MaxValue));
            Debug.Log($"Using Random seed {randomSeed}");

            GenerateMonster(randomSeed);
        }

        public MonsterCollection GenerateMonster(int randomSeed)
        {
            var ranchMonsterCollection = new MonsterCollection(ranchMonsters);
            var randomGenerator = new System.Random(randomSeed);

            Debug.Log("Generate Monster");
            var newMonster = new Monster()
            {
                age = 0,
                displayName = "Select Monster Name",
                hp = 0,
                defense = new MonsterStat("Defense"),
                intellect = new MonsterStat("Intellect"),
                strength = new MonsterStat("Strength"),
                toughness = new MonsterStat("Toughness")
            };

            var monsterStats = new MonsterStat[]{
                newMonster.defense,
                newMonster.intellect,
                newMonster.strength,
                newMonster.toughness
            };

            //TODO replace with more interesting balancing of weights
            var simpleStatWeightDistribution = (new double[]{
                0.7,
                0.4,
                0.3,
                0.9
            }).OrderBy(x => randomGenerator.Next()).ToArray();

            for (var i = 0; i < monsterStats.Length; i++)
            {

                monsterStats[i].potentialStrength = simpleStatWeightDistribution.First();
                monsterStats[i].currentValue = monsterStats[i].potentialStrength / 10 + randomGenerator.NextDouble() / 2;
            }
            ranchMonsterCollection.list.Add(newMonster);
            return ranchMonsterCollection;
        }

    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using ZeroFormatter;

namespace RancherShared
{

    [System.Serializable]
    [ZeroFormattable]
     public class TournamentCollection {
         [Index(0)]
         public virtual List<Tournament> TournamentList { get; set;} = new List<Tournament>(1);
     }

    [System.Serializable]
    [ZeroFormattable]
    public class Tournament
    {
        [Index(0)]
        public virtual string Id { get; set;}

    }
}


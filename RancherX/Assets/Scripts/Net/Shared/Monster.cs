using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZeroFormatter;

namespace RancherShared
{

    [System.Serializable]
    [ZeroFormattable]
     public class MonsterCollection {
         [Index(0)]
         public virtual List<Monster> list { get; set;} = new List<Monster>(1);

        public MonsterCollection(){}
         public MonsterCollection(MonsterCollection _initialMonsterCollection){
             list.AddRange(_initialMonsterCollection.list);
         }
     }

    [System.Serializable]
    [ZeroFormattable]
    public class Monster
    {

        [Index(0)]
        public virtual Double hp { get; set;}
        [Index(1)]
        public virtual Double age { get; set; }
        [Index(2)]
        public virtual String displayName { get; set; }
        [Index(3)]
        public virtual MonsterStat intellect { get; set; }
        [Index(4)]
        public virtual MonsterStat strength { get; set; }
        [Index(5)]
        public virtual MonsterStat defense { get; set; }
        [Index(6)]
        public virtual MonsterStat toughness { get; set; }
        [Index(7)]
        public virtual string guid  { get; set; }

    }


    [System.Serializable]
    [ZeroFormattable]
    public class MonsterStat {

        public MonsterStat(){
            
        }
        public MonsterStat(string name){
            currentValue = 0;
            potentialStrength = 0;
            displayName = name;

        }
        [Index(0)]
        public virtual string displayName { get; set; }
        [Index(1)]
        public virtual Double currentValue { get; set; }
        [Index(2)]
        public virtual Double potentialStrength { get; set; }

        public override string ToString()
        {
            return $"{displayName}: currentValue {currentValue} potentialStrength {potentialStrength}";
        }
    }
}


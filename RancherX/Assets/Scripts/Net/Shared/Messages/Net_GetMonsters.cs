using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Networking.Transport;
using UnityEngine;
using ZeroFormatter;

namespace RancherShared
{


    public class Net_GetMonsters : NetMessage
    {
        public MonsterCollection _monsterPayload;

        public Net_GetMonsters(ref DataStreamReader streamReader, string _guid) : base(streamReader, _guid)
        {
            Code = OpCode.GetMonsterCollection;
        }
        public Net_GetMonsters(MonsterCollection monstersPayload, string _guid)
        {
            Code = OpCode.GetMonsterCollection;
            _monsterPayload = monstersPayload;
            guid = _guid;
        }

        public Net_GetMonsters() : base()
        {
            Code = OpCode.GetMonsterCollection;
        }

        public override void SerializeToClient(ref Unity.Networking.Transport.DataStreamWriter writer)
        {
            base.SerializeToServer(ref writer);
            var serializedCollection = ZeroFormatterSerializer.Serialize(_monsterPayload);
            using (var serializedNativeArray = new NativeArray<byte>(serializedCollection, Allocator.Persistent))
            {
                short monsterByteLegnth = (short)serializedNativeArray.Length;
                writer.WriteShort(monsterByteLegnth);
                writer.WriteBytes(serializedNativeArray);
            }
        }

        public override void DeSerializeOnClient(ref DataStreamReader streamReader)
        {
            short monsterByteLegnth = streamReader.ReadShort();
            using (NativeArray<byte> monsterBytes = new NativeArray<byte>(monsterByteLegnth, Allocator.Persistent, NativeArrayOptions.ClearMemory))
            {
                streamReader.ReadBytes(monsterBytes);
                _monsterPayload = ZeroFormatterSerializer.Deserialize<MonsterCollection>(monsterBytes.ToArray());
            }
        }

        public override void SerializeToServer(ref Unity.Networking.Transport.DataStreamWriter writer)
        {
            base.SerializeToServer(ref writer);
           
        }

        public override void ReceivedOnServer()
        {
            base.ReceivedOnServer();
            UnityEngine.Debug.Log($"SERVER: Received get Monster ");
        }


        public override void ReceivedOnClient()
        {
            base.ReceivedOnClient();
            UnityEngine.Debug.Log($"CLIENT: Received Net_GetMonsters payload:  {_monsterPayload}");
        }
        public override object GetPayloadFromServer()
        {
            return _monsterPayload;
        }

    }
}
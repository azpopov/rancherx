using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Networking.Transport;
using UnityEngine;
using ZeroFormatter;


namespace RancherShared
{


    public class Net_GenerateMonsterFromSeed : NetMessage
    {
        public int _monsterSeed;
        public MonsterCollection _monstersPayload;
        private DataStreamReader streamReader;
        public Net_GenerateMonsterFromSeed(ref DataStreamReader streamReader, string _guid) : base(streamReader, _guid)
        {
            Code = OpCode.GenerateMonsterFromSeed;
        }

        public Net_GenerateMonsterFromSeed(int monsterSeed)
        {
            Code = OpCode.GenerateMonsterFromSeed;
            _monsterSeed = monsterSeed;

        }

        public Net_GenerateMonsterFromSeed(MonsterCollection monstersPayload, string _guid)
        {
            Code = OpCode.GenerateMonsterFromSeed;
            _monstersPayload = monstersPayload;
            guid = _guid;

        }

        public override void SerializeToClient(ref Unity.Networking.Transport.DataStreamWriter writer)
        {
            base.SerializeToClient(ref writer);
            var serializedMonsterCollection = ZeroFormatterSerializer.Serialize(_monstersPayload);
            using(var serializedNativeArray = new NativeArray<byte>(serializedMonsterCollection, Allocator.Persistent)){
                short monsterByteLegnth = (short)serializedNativeArray.Length;
                writer.WriteShort(monsterByteLegnth);
                writer.WriteBytes(serializedNativeArray);
            }
        }

        public override void DeSerializeOnClient(ref DataStreamReader streamReader)
        {
            short monsterByteLegnth = streamReader.ReadShort();
            using(NativeArray<byte> monsterBytes = new NativeArray<byte>(monsterByteLegnth, Allocator.Persistent, NativeArrayOptions.ClearMemory)){
                streamReader.ReadBytes(monsterBytes);
                _monstersPayload = ZeroFormatterSerializer.Deserialize<MonsterCollection>(monsterBytes.ToArray());
            }
        }

        public override void SerializeToServer(ref Unity.Networking.Transport.DataStreamWriter writer)
        {
            base.SerializeToServer(ref writer);
            writer.WriteInt(_monsterSeed);
        }

        public override void DeSerializeOnServer(ref DataStreamReader streamReader)
        {
            base.DeSerializeOnServer(ref streamReader);
            _monsterSeed = streamReader.ReadInt();
        }

        public override void ReceivedOnServer()
        {
            base.ReceivedOnServer();
            UnityEngine.Debug.Log($"Received _monsterSeed {_monsterSeed}");
        }


        public override void ReceivedOnClient()
        {
            base.ReceivedOnClient();
            UnityEngine.Debug.Log($"Received _monsterList {_monstersPayload}");
        }

        public override object GetPayloadFromClient<Int32>()
        {
            return _monsterSeed;
        }

        public override object GetPayloadFromServer()
        {
            return _monstersPayload;
        }

    }
}
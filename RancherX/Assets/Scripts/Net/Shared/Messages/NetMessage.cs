using UnityEngine;
using Unity.Networking.Transport;
using System;
using Unity.Collections;

namespace RancherShared
{

    public class NetMessage
    {
        public NetMessage(){
            guid = Guid.NewGuid().ToString();
        }
        public NetMessage(DataStreamReader streamReader, string _guid)
        {
            guid = _guid;
        }
        public OpCode Code { get; set; }
        public string guid {get; set;}

        public virtual void SerializeToServer(ref DataStreamWriter writer) {
            writer.WriteByte((byte)Code);
            using(var guidNativeArray = new NativeArray<byte>(System.Text.Encoding.ASCII.GetBytes(guid), Allocator.Persistent)){
                writer.WriteUShort((ushort)guidNativeArray.Length);
                writer.WriteBytes(guidNativeArray);
            }
         }

        public virtual void DeSerializeOnServer(ref DataStreamReader streamReader) {
         }
        public virtual void SerializeToClient(ref DataStreamWriter writer) {
              writer.WriteByte((byte)Code);
            using (var guidNativeArray = new NativeArray<byte>(System.Text.Encoding.ASCII.GetBytes(guid), Allocator.Persistent))
            {
                writer.WriteUShort((ushort)guidNativeArray.Length);
                writer.WriteBytes(guidNativeArray);
            }
         }

        public virtual void DeSerializeOnClient(ref DataStreamReader streamReader) { }


        public virtual void ReceivedOnServer()
        {
            Debug.Log("Received on Server");
        }

        public virtual void ReceivedOnClient()
        {
            Debug.Log("Received on Client");
        }

        public virtual object GetPayloadFromClient<T>()
        {
            throw new NotImplementedException();
        }

        public virtual object GetPayloadFromServer(){
            throw new NotImplementedException();
        }
    }
}
namespace RancherShared
{

    public enum OpCode
    {
        MonstersUpdate = 1,
        
        GenerateMonsterFromSeed = 2,
        GetTournaments = 3,
        DeleteMonster = 4,
        GetMonsterCollection = 5
    }
}
using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Networking.Transport;
using UnityEngine;
using ZeroFormatter;

namespace RancherShared
{


    public class Net_GetTournaments : NetMessage
    {
        public TournamentCollection _tournamentPayload;

        public Net_GetTournaments(ref DataStreamReader streamReader, string _guid) : base(streamReader, _guid)
        {
            Code = OpCode.GetTournaments;
        }
        public Net_GetTournaments(TournamentCollection monstersPayload) : base()
        {
            Code = OpCode.GetTournaments;
            _tournamentPayload = monstersPayload;

        }

        public Net_GetTournaments() : base()
        {
            Code = OpCode.GetTournaments;
        }

        public override void SerializeToClient(ref Unity.Networking.Transport.DataStreamWriter writer)
        {
            base.SerializeToServer(ref writer);
            var serializedTournamentCollection = ZeroFormatterSerializer.Serialize(_tournamentPayload);
            using (var serializedNativeArray = new NativeArray<byte>(serializedTournamentCollection, Allocator.Persistent))
            {
                short monsterByteLegnth = (short)serializedNativeArray.Length;
                writer.WriteShort(monsterByteLegnth);
                writer.WriteBytes(serializedNativeArray);
            }
        }

        public override void DeSerializeOnClient(ref DataStreamReader streamReader)
        {
            short monsterByteLegnth = streamReader.ReadShort();
            using (NativeArray<byte> monsterBytes = new NativeArray<byte>(monsterByteLegnth, Allocator.Persistent, NativeArrayOptions.ClearMemory))
            {
                streamReader.ReadBytes(monsterBytes);
                _tournamentPayload = ZeroFormatterSerializer.Deserialize<TournamentCollection>(monsterBytes.ToArray());
            }
        }

        public override void SerializeToServer(ref Unity.Networking.Transport.DataStreamWriter writer)
        {
            base.SerializeToServer(ref writer);
           
        }

        public override void ReceivedOnServer()
        {
            base.ReceivedOnServer();
            UnityEngine.Debug.Log($"Received tournament request");
        }


        public override void ReceivedOnClient()
        {
            base.ReceivedOnClient();
            UnityEngine.Debug.Log($"Received _tournamentPayload {_tournamentPayload}");
        }
        public override object GetPayloadFromServer()
        {
            return _tournamentPayload;
        }

    }
}
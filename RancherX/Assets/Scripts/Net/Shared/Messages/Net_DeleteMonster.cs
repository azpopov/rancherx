using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Networking.Transport;
using UnityEngine;
using ZeroFormatter;


namespace RancherShared 
{

    
    public class Net_DeleteMonster : NetMessage
    {
        public string monsterGuid;
        public MonsterCollection _monstersPayload;
        public Net_DeleteMonster(ref DataStreamReader streamReader, string _guid) : base(streamReader, _guid)
        {
            Code = OpCode.DeleteMonster;
        }

        public Net_DeleteMonster(string _monsterGuid)
        {
            Code = OpCode.DeleteMonster;
            monsterGuid = _monsterGuid;
        }

          public Net_DeleteMonster(MonsterCollection monstersPayload, string _guid)
        {
            Code = OpCode.DeleteMonster;
            _monstersPayload = monstersPayload;
            guid = _guid;

        }

        public override void SerializeToClient(ref Unity.Networking.Transport.DataStreamWriter writer)
        {
            base.SerializeToClient(ref writer);
            var serializedMonsterCollection = ZeroFormatterSerializer.Serialize(_monstersPayload);
            var serializedNativeArray = new NativeArray<byte>(serializedMonsterCollection, Allocator.Persistent);
            short monsterByteLegnth = (short)serializedNativeArray.Length; 
            writer.WriteShort(monsterByteLegnth);
            writer.WriteBytes(serializedNativeArray);
            serializedNativeArray.Dispose();
        }

        public override void DeSerializeOnClient(ref DataStreamReader streamReader)
        {
            short monsterByteLegnth = streamReader.ReadShort(); 
            NativeArray<byte> monsterBytes = new NativeArray<byte>(monsterByteLegnth, Allocator.Persistent,NativeArrayOptions.ClearMemory);
            streamReader.ReadBytes(monsterBytes);
            _monstersPayload = ZeroFormatterSerializer.Deserialize<MonsterCollection>(monsterBytes.ToArray());
            monsterBytes.Dispose();
        }

        public override void SerializeToServer(ref Unity.Networking.Transport.DataStreamWriter writer) {
            base.SerializeToServer(ref writer);
        }

        public override void DeSerializeOnServer(ref DataStreamReader streamReader)
        {
            base.DeSerializeOnServer(ref streamReader);
        }

        public override void ReceivedOnServer() {
            base.ReceivedOnServer();
            UnityEngine.Debug.Log($"Received _monsterSeed {monsterGuid}");
        }

        
        public override void ReceivedOnClient() {
            base.ReceivedOnClient();
            UnityEngine.Debug.Log($"Received _monsterList {_monstersPayload}");
        }

        public override object GetPayloadFromClient<FixedString128>(){
            return monsterGuid;
        }

        public override object GetPayloadFromServer(){
            return _monstersPayload;
        }
        
    }
    }
using UnityEngine;
using Unity.Networking.Transport;
using Unity.Collections;
using System.Collections;
using System.Collections.Generic;
using RancherShared;
using System.Linq;
using System;
using Codice.Client.Commands;

namespace RancherClient
{

    public class RequestStatus
    {
        public RequestStatus(string _guid, System.Action<System.Object> _callback)
        {
            guid = _guid;
            callBack = _callback;
        }
        public long killTime = (BaseClient.getTime() + 60);
        public string guid;

        public System.Action<System.Object> callBack;

    }

    public class BaseClient : MonoSingleton<BaseClient>
    {
        private static DateTime JanFirst1970 = new DateTime(1970, 1, 1);
        public static long getTime()
        {
            return (long)((DateTime.Now.ToUniversalTime() - JanFirst1970).TotalMilliseconds + 0.5);
        }

        class QueuedNetMessage
        {
            internal QueuedNetMessage(NetMessage _netMessage, System.Action<System.Object> _callback)
            {
                netMessage = _netMessage;
                callBack = _callback;
                killTime = (BaseClient.getTime() + 150) * 10000;
            }
            public long killTime;
            public NetMessage netMessage;

            public System.Action<System.Object> callBack;

        }

        private List<QueuedNetMessage> netMessageQueue = new List<QueuedNetMessage>();
        public NetworkDriver driver;
        protected NetworkConnection connection;
        private List<RequestStatus> requestStatusList;
#if UNITY_EDITOR
        private void Awake()
        {
            Init();
        }
        private void Update()
        {
            UpdateServer();
        }
        private void OnDestroy()
        {
            ShutDown();
        }
#endif

        NetworkEndPoint endPoint;
        public virtual void Init()
        {
            requestStatusList = new List<RequestStatus>();
            driver = NetworkDriver.Create();
            connection = default(NetworkConnection);
            endPoint = NetworkEndPoint.LoopbackIpv4;
            endPoint.Port = 5522;
            connection = driver.Connect(endPoint);
        }

        public virtual void UpdateServer()
        {
            driver.ScheduleUpdate().Complete();
            CheckAlive();
            UpdateMessagePump();
            while (netMessageQueue.Count > 0 && connection.IsCreated && connection.GetState(driver) == NetworkConnection.State.Connected)
            {
                QueuedNetMessage queuedMessge = netMessageQueue[0];
                PerformSendToServer(queuedMessge.netMessage, queuedMessge.callBack);
                netMessageQueue.RemoveAt(0);
            }
        }

        public virtual void ShutDown()
        {
            driver.Dispose();
        }

        private void CheckAlive()
        {
            if (!connection.IsCreated)
            {
                Debug.Log($"Lost connection");
                connection = default(NetworkConnection);
                connection = driver.Connect(endPoint);
            }
        }

        protected virtual void UpdateMessagePump()
        {
            DataStreamReader streamReader;

            NetworkEvent.Type cmd;
            while ((cmd = connection.PopEvent(driver, out streamReader)) != NetworkEvent.Type.Empty)
            {
                if (cmd == NetworkEvent.Type.Connect)
                {
                    Debug.Log("We are now connected to the server");
                }
                else if (cmd == NetworkEvent.Type.Data)
                {
                    OnData(ref streamReader);
                }
                else if (cmd == NetworkEvent.Type.Disconnect)
                {
                    Debug.Log("Cleitn has Disconnected from server");
                    connection = default(NetworkConnection);
                }
            }
        }
        public virtual void SendToServer(NetMessage message, System.Action<System.Object> callback)
        {
            netMessageQueue.Add(new QueuedNetMessage(message, callback));
        }

        private void PerformSendToServer(NetMessage message, Action<System.Object> callback)
        {
            AddCallbackToList(message, callback);
            DataStreamWriter writer;
            driver.BeginSend(connection, out writer);
            message.SerializeToServer(ref writer);
            driver.EndSend(writer);
        }

        private void AddCallbackToList(NetMessage message, Action<System.Object> callback)
        {
            var requestStatus = new RequestStatus(message.guid, callback);
            requestStatusList.Add(requestStatus);
        }

        public virtual void OnData(ref DataStreamReader streamReader)
        {
            NetMessage netMessage = null;
            var opCode = (OpCode)streamReader.ReadByte();
            short guidLength = streamReader.ReadShort();
            using (NativeArray<byte> monsterGuid = new NativeArray<byte>(guidLength, Allocator.Persistent, NativeArrayOptions.ClearMemory))
            {
                streamReader.ReadBytes(monsterGuid);
                var byteGuidArr = NativeArrayExtension.ToRawBytes(monsterGuid);
                var guid = System.Text.Encoding.ASCII.GetString(byteGuidArr, 0, guidLength);
                switch (opCode)
                {
                    case OpCode.GenerateMonsterFromSeed:
                        netMessage = new Net_GenerateMonsterFromSeed(ref streamReader, guid);
                        netMessage.DeSerializeOnClient(ref streamReader);
                        break;
                    case OpCode.GetTournaments:
                        netMessage = new Net_GetTournaments(ref streamReader, guid);
                        netMessage.DeSerializeOnClient(ref streamReader);
                        break;
                    case OpCode.DeleteMonster:
                        netMessage = new Net_DeleteMonster(ref streamReader, guid);
                        netMessage.DeSerializeOnClient(ref streamReader);
                        break;
                    case OpCode.GetMonsterCollection:
                        netMessage = new Net_GetMonsters(ref streamReader, guid);
                        netMessage.DeSerializeOnClient(ref streamReader);
                        break;
                    default:
                        break;
                }
                var request = requestStatusList.Find(item => { return item.guid == guid; });
                if (request != null)
                {
                    request.callBack(netMessage.GetPayloadFromServer());
                }
                requestStatusList.RemoveAll(item => item.killTime < (BaseClient.getTime()));

                netMessage.ReceivedOnClient();
                Debug.Log($"OpCode: {opCode}");
            }
        }
    }
}

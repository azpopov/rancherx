 
using UnityEngine;
using UnityEditor;
 
[InitializeOnLoad]
public class AutoPlayer : MonoBehaviour
{
    const string MenuName_EnterPlaymodeAfterCompile = "Autoplay/Enter Playmode after Compile";
 
    public static bool EnterPlaymodeAfterCompile
    {
        get { return EditorPrefs.GetBool( "EnterPlaymodeAfterCompile", false ); }
        set { EditorPrefs.SetBool( "EnterPlaymodeAfterCompile", value ); }
    }
 
    [MenuItem( MenuName_EnterPlaymodeAfterCompile )]
    static void ToggleEnterPlaymodeAfterCompile()
    {
        EnterPlaymodeAfterCompile = !EnterPlaymodeAfterCompile;
    }
 
    [MenuItem( MenuName_EnterPlaymodeAfterCompile, true )]
    static bool ValidateToggleEnterPlaymodeAfterCompile()
    {
        UnityEditor.Menu.SetChecked( MenuName_EnterPlaymodeAfterCompile, EnterPlaymodeAfterCompile );
        return true;
    }
 
    [UnityEditor.Callbacks.DidReloadScripts]
    private static void OnScriptsCompiled()
    {
        if ( !Application.isPlaying && EnterPlaymodeAfterCompile )
        {
            EditorApplication.EnterPlaymode();
        }
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using RancherShared;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace RancherFront{
public class MonsterStableListElement : MonoBehaviour
{
    public Monster monsterData;
    public Button monsterButton; 
    Text text;
    // Start is called before the first frame update
    void Start()
    {
        monsterButton = transform.GetComponentInChildren<Button>();
        monsterButton.onClick.AddListener(() => SelectMonster(monsterData));
        text = transform.GetComponentInChildren<Text>();
        text.text = monsterData.displayName;
    }

        void SelectMonster(Monster _monsterData)
        {
            StableManager.onChangeMonster(this.monsterData);
        }

        // Update is called once per frame
        void Update()
    {
        
    }
}
}

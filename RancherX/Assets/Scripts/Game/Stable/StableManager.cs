using System;
using System.Collections;
using System.Collections.Generic;
using RancherClient;
using RancherShared;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace RancherFront
{
    public class StableManager : PartialSceneManager
    {
        public delegate void OnChangeMonster(Monster _monster);
        public static OnChangeMonster onChangeMonster;
        public GameObject monsterPrefab;
        public GameObject statPrefab;
        private List<GameObject> monsterGameobjects = null;
        private List<GameObject> statPanels = null;

        private Text selectedMonsterTitle;
        private Transform stableStatListTransform;

        private Transform monsterAvatarListTransform;
        private GameObject[] monsterDataGameObjects;

        public override void Awake()
        {
            base.Awake();
            statPanels = new List<GameObject>();
            onChangeMonster += ChangeTitle;
            onChangeMonster += ChangeMonsterStats;
            monsterDataGameObjects = GameObject.FindGameObjectsWithTag("MonsterData");

            stableStatListTransform = Array.Find<GameObject>(monsterDataGameObjects, (_el) =>
            {
                return _el.transform.name == "StatPanel";
            }).transform;
        }

        private void Start()
        {
            ManagerScript.instance.client.SendToServer(new Net_GetMonsters(), OnStableMonsterListUpdate);
        }

        private void OnStableMonsterListUpdate(object listUpdateResponse)
        {
            ManagerScript.instance.monsterService.AssignMonsters(listUpdateResponse);
            FillStableList();
            var selectedMonster = ManagerScript.instance.monsterService.monsters.list;
            onChangeMonster(selectedMonster.Count > 0 ? selectedMonster[0] : new Monster() );
        }

        private void ChangeTitle(Monster _monster)
        {
            var selectedMonsterTitleGameObject = Array.Find<GameObject>(ManagerScript.instance.ui.monsterStatElements.ToArray(), (_el) =>
            {
                return _el.name == "SelectedMonsterName";
            });
            selectedMonsterTitle = selectedMonsterTitleGameObject.GetComponent<Text>();
            selectedMonsterTitle.text = _monster.displayName;
        }

        public void OnDestroy()
        {
            onChangeMonster -= ChangeTitle;
            onChangeMonster -= ChangeMonsterStats;
        }

        private void ChangeMonsterStats(Monster _monster)
        {
            var monsterController = new MonsterController(_monster);
            var monsterStatListToShow = monsterController.GetStableStats();
            MonsterStat monsterStat = monsterStatListToShow.Current;

            while (monsterStatListToShow.MoveNext())
            {
                monsterStat = monsterStatListToShow.Current;
                GameObject statPanel = statPanels.Find(panel => panel.GetComponentInChildren<Text>().text == monsterStat.displayName);
                if (statPanel == null)
                {
                    statPanel = Instantiate(statPrefab, new Vector3(UnityEngine.Random.Range(-5, 5), UnityEngine.Random.Range(-5, 5), 0), Quaternion.identity);
                    statPanel.transform.SetParent(stableStatListTransform);
                    statPanel.GetComponentInChildren<Text>().text = monsterStat.displayName;
                    statPanels.Add(statPanel);
                }
                var slider = statPanel.GetComponentInChildren<Slider>();
                slider.value = (float)monsterStat.currentValue;
            }
            var monsterRetireButton = Array.Find<GameObject>(ManagerScript.instance.ui.monsterStatElements.ToArray(), (_el) =>
            {
                return _el.name == "RetireMonsterButton";
            });
            monsterRetireButton.GetComponent<Button>().onClick.AddListener(() => RetireMonster(_monster.guid));
        
        }

        private void RetireMonster(string guid)
        {
            BaseClient.instance.SendToServer(new Net_DeleteMonster(guid), ManagerScript.instance.monsterService.AssignMonsters);
        }

        private void FillStableList()
        {
            monsterAvatarListTransform = GameObject.FindGameObjectWithTag("List").transform;
            if (monsterGameobjects == null)
            {
                monsterGameobjects = new List<GameObject>(1);
                ManagerScript.instance.monsterService.monsters.list.ForEach(ranchMonster =>
                {
                    Debug.Log($"Spawning Monster {ranchMonster}");
                    var newMonsterGameObject = Instantiate(monsterPrefab, new Vector3(UnityEngine.Random.Range(-5, 5), UnityEngine.Random.Range(-5, 5), 0), Quaternion.identity);
                    newMonsterGameObject.transform.SetParent(monsterAvatarListTransform);
                    newMonsterGameObject.GetComponent<MonsterStableListElement>().monsterData = ranchMonster;
                    monsterGameobjects.Add(newMonsterGameObject);
                });
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RancherFront
{
    public class CalendarToggleButton : MonoBehaviour
    {

        // Start is called before the first frame update
        void Start()
        {
            var button = GetComponent<Button>();
            button.onClick.AddListener(() => ManagerScript.instance.transitionManager.ToggleCalendar());
        }


    }
}

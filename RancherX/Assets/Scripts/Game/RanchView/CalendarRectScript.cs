using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RancherFront{ 

public class CalendarRectScript : MonoBehaviour
{


    float timeOfTravel = 1; //time after object reach a target place 
    float currentTime = 0; // actual floting time 
    float normalizedValue;
    RectTransform rectTransform;

    Vector3 startPosition;
    Vector3 endPosition;

    IEnumerator LerpObject()
    {

        while (currentTime <= timeOfTravel)
        {
            currentTime += Time.deltaTime;
            normalizedValue = currentTime / timeOfTravel; // we normalize our time 

            rectTransform.anchoredPosition = new Vector3(
                Mathf.SmoothStep(startPosition.x, endPosition.x, normalizedValue),
                Mathf.SmoothStep(startPosition.y, endPosition.y, normalizedValue), 0);
            yield return null;
        }
    }

    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    public void TransitionInVertical()
    {
        var startPosition = new Vector2(275.0f, 500.0f);
        var endPosition = new Vector2(275.0f, 60.5f);
        var rectTransform = transform.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = startPosition;
        this.startPosition = startPosition;
        this.endPosition = endPosition;
        this.currentTime = 0;
        IEnumerator cachedCoroutine = LerpObject();
        StartCoroutine(cachedCoroutine);
    }

    public void TransitionOutVertical()
    {
        var startPosition = new Vector2(275.0f, 60.5f);
        var endPosition = new Vector2(275.0f, 500.0f);
        var rectTransform = transform.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = startPosition;
        this.startPosition = startPosition;
        this.endPosition = endPosition;
        this.currentTime = 0;
        IEnumerator cachedCoroutine = LerpObject();
        StartCoroutine(cachedCoroutine);
    }
}
}

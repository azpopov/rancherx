using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RancherFront
{
    public class RanchOverviewManager : PartialSceneManager
    {
        public GameObject navigationButton;
        public GameObject monster;
        private List<GameObject> monsterGameobjects;
        public override void Awake()
        {
            base.Awake();
            SpawnMonsters();
            BindNavigationButtons();
            BindCalendarButton();
        }

        private void BindCalendarButton()
        {
            
        }

        private void BindNavigationButtons()
        {
           var navigationButtonContainer = Array.Find(GameObject.FindGameObjectsWithTag("List"), (el) => {return el.transform.name == "NavigationButtons";});
           var ranchNavigationButtonConfigurations = RanchOverviewNavigation.NavigationButtons;
           for(var i = 0; i <= ranchNavigationButtonConfigurations.GetUpperBound(0); i++){
               var buttonName = ranchNavigationButtonConfigurations[i, 0];
               var buttonLoadingScene = ranchNavigationButtonConfigurations[i,1];
               var buttonTransform = Instantiate(navigationButton, navigationButtonContainer.transform);
               var buttonText = buttonTransform.GetComponentInChildren<Text>();
               buttonText.text = buttonName;
               var button = buttonTransform.GetComponent<Button>();
               button.onClick.AddListener(() => transitionManager.LoadScene(buttonLoadingScene));

           }
        }

        void SpawnMonsters()
        {
            monsterGameobjects = new List<GameObject>(1);
            ManagerScript.instance.monsterService.monsters.list.ForEach(ranchMonster =>
            {
                Debug.Log($"Spawning Monster {ranchMonster}");
                monsterGameobjects.Add(Instantiate(monster, new Vector3(UnityEngine.Random.Range(-5, 5), UnityEngine.Random.Range(-5, 5), 0), Quaternion.identity));
            });
        }
    }
}
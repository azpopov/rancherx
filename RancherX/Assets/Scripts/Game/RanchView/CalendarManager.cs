using System.Collections;
using System.Collections.Generic;
using RancherShared;
using UnityEngine;
using UnityEngine.UI;

namespace RancherFront
{

    public class CalendarManager : MonoBehaviour
    {


        public GameObject dayObject;
        RectTransform rectTransform;
        System.DateTime now;
        // Start is called before the first frame update
        void Start()
        {
            now = System.DateTime.Now;
            var daysInMonth = System.DateTime.DaysInMonth(now.Year, now.Month);
            rectTransform = GetComponent<RectTransform>();
            var gridLayout = transform.GetComponentInChildren<GridLayoutGroup>();
            for (var i = 0; i < daysInMonth; i++)
            {
                var calendarDay = GameObject.Instantiate(dayObject, new Vector3(0f, 0f, 0f), Quaternion.identity);
                calendarDay.transform.SetParent(gridLayout.transform);
                calendarDay.GetComponentInChildren<Text>().text = $"{i + 1}";
            }
            var generatedMonsterNetMessage = new Net_GenerateMonsterFromSeed(123);

            RancherClient.BaseClient.instance.SendToServer(generatedMonsterNetMessage, UpdateTournaments);
            ManagerScript.instance.client.SendToServer(new Net_GetTournaments(), UpdateTournaments);
        }

        private void UpdateTournaments(object obj)
        {
            Debug.Log(obj);
        }

        // Update is called once per frame
        void Update()
        {


        }
    }

}

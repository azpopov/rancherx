namespace RancherFront{
    public static class RanchOverviewNavigation{ 

        public static string[,] NavigationButtons = new string[,] {
            {"Stable", "stable"},
            {"Nursery", "nursery"},
        };
    }

}

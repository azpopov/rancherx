using Unity;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using RancherShared;

namespace RancherFront {
    public partial class PartialSceneManager : MonoBehaviour {

        internal TransitionManager transitionManager;
        public GameObject backButton;

        protected MonsterService monsterListGameObject;
        public virtual void Awake(){
            GameObject staticGameManagers = GameObject.FindGameObjectWithTag("GameManager");
            transitionManager = staticGameManagers.GetComponentInChildren<TransitionManager>();
            if (SceneManager.GetActiveScene() != SceneManager.GetSceneByBuildIndex(0)){
                var canvas = GameObject.FindObjectOfType<Canvas>();
                var backButtonInstance = Instantiate(backButton);
                backButtonInstance.transform.SetParent(canvas.transform, false);
                backButtonInstance.GetComponent<Button>().onClick.AddListener(() => transitionManager.OnPreviousScene());
            }
        }
    }
}
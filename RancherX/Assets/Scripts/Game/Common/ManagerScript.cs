using System.Collections;
using System.Collections.Generic;
using RancherClient;
using RancherShared;
using UnityEngine;

namespace RancherFront
{
    public class ManagerScript : MonoBehaviour
    {
        public TransitionManager transitionManager; 
        public static ManagerScript instance;
        public UIManager ui;

        public RancherNetEventManager netEventManager;

        public MonsterService monsterService  = new MonsterService();
        public BaseClient client;
        void Awake()
        {
            if(instance == null){
                instance = this;
                DontDestroyOnLoad (gameObject);     
                client = gameObject.AddComponent<BaseClient>();
                
            }
            else
                Destroy (gameObject);
        }
    }
}




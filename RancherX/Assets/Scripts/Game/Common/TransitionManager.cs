﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RancherFront
{
    public class TransitionManager : MonoBehaviour
    {        
        public static Stack<string> sceneHistory = null;
        public void LoadScene(string sceneName) {
            Debug.Log("LoadScene");
            StartCoroutine(ChangeScene(sceneName));
        }

        public void Awake() {

            ManagerScript.instance.transitionManager = this;
            if(TransitionManager.sceneHistory == null){
               sceneHistory = new Stack<string>();
               sceneHistory.Push(SceneManager.GetActiveScene().name);
            }
        }

        public void Start(){
            Debug.Log("startred trans");
        }

         public void ToggleCalendar() {
            Debug.Log("ToggleCalendar");
            CalendarRectScript calendarGameobject = GameObject.FindObjectOfType<CalendarRectScript>();
            var vectorOffsets = GetGUIElementOffset(calendarGameobject.GetComponent<RectTransform>());
            var cornersOffScreen = Math.Abs(vectorOffsets.x) > 0 ? 1 : 0 +  Math.Abs(vectorOffsets.y) > 0 ? 1 : 0; 
            if(cornersOffScreen == 0){
                calendarGameobject.TransitionOutVertical();
            } else {
                calendarGameobject.TransitionInVertical();
            }
        }

        internal void OnPreviousScene()
        {
            TransitionManager.sceneHistory.Pop();
            StartCoroutine(ChangeScene(TransitionManager.sceneHistory.Pop()));
        }

        IEnumerator ChangeScene(string sceneName) {
            TransitionManager.sceneHistory.Push(sceneName);

            AsyncOperation operation = SceneManager.LoadSceneAsync($"Scenes/{sceneName}");
            operation.allowSceneActivation = false;
            
            var updateTime = new WaitForSeconds(0.001f);
            
            while (!operation.isDone) {
                if (operation.progress >= 0.2f) {

                    operation.allowSceneActivation = true;
                }
                yield return updateTime;
            }
        }
        public static Vector3 GetGUIElementOffset(RectTransform rect)
    {
        Rect screenBounds = new Rect(0f, 0f, Screen.width, Screen.height);
        Vector3[] objectCorners = new Vector3[4];
        rect.GetWorldCorners(objectCorners);
 
        var xnew = 0f;
        var ynew = 0f;
        var znew = 0f;
 
        for (int i = 0; i < objectCorners.Length; i++)
        {
            if (objectCorners[i].x < screenBounds.xMin)
            {
                xnew = screenBounds.xMin - objectCorners[i].x;
            }
            if (objectCorners[i].x > screenBounds.xMax)
            {
                xnew = screenBounds.xMax - objectCorners[i].x;
            }
            if (objectCorners[i].y < screenBounds.yMin)
            {
                ynew = screenBounds.yMin - objectCorners[i].y;
            }
            if (objectCorners[i].y > screenBounds.yMax)
            {
                ynew = screenBounds.yMax - objectCorners[i].y;
            }
        }
 
        return new Vector3(xnew, ynew, znew);
 
    }
    }
}
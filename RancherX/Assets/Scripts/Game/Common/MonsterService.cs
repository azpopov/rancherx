using System;
using System.Collections.Generic;
using RancherShared;

namespace RancherFront
{
    public class MonsterService
    {
        public MonsterCollection monsters = new MonsterCollection();

        public void AssignMonsters(object _monsterCollection)
        {
            monsters = (MonsterCollection)_monsterCollection;
        }

        public void UpdateMonsters (){
            var updateMonstersNetMessage = new Net_GetMonsters();
            ManagerScript.instance.client.SendToServer(new Net_GetTournaments(), AssignMonsters);
        }
    }
}

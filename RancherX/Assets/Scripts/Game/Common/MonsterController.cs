using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using RancherShared;

namespace RancherFront
{
    public class MonsterController
    {
        private Monster monster;
        public MonsterController(Monster _monster)
        {
            monster = _monster;
        }

        public IEnumerator<MonsterStat> GetStableStats()
        {
            yield return monster.intellect;
            yield return monster.strength;
            yield return monster.defense;
            yield return monster.toughness;
        }
    }

    
}


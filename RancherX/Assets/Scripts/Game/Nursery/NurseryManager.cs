using System;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using Unity.Collections;
using RancherShared;
using System.Globalization;

namespace RancherFront
{
    public class NurseryManager : PartialSceneManager
    {
        public GameObject generateFromSeedPopup;
        public GameObject canvas;
        public void DisplayGenerateFromSeedPopup()
        {

            var popup = Instantiate(generateFromSeedPopup, canvas.transform);
            popup.GetComponentInChildren<Button>().onClick.AddListener(() =>
            {
                var inputText = popup.GetComponentInChildren<InputField>().text;
                int.TryParse(inputText, out int inputInt);
                var generatedMonsterNetMessage = new Net_GenerateMonsterFromSeed(inputInt);

                RancherClient.BaseClient.instance.SendToServer(generatedMonsterNetMessage, AssignMonster);
                
                Destroy(popup);
            });
        }

        private void AssignMonster(object response) {
            ManagerScript.instance.monsterService.monsters = (MonsterCollection)response;
        }

        private void RetireMonster(){
            
        }

       
    }
}
